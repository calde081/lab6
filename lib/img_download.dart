import 'dart:async';
import 'package:flutter/material.dart';
import 'dart:io';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker_saver/image_picker_saver.dart';
import 'package:http/http.dart' as http;
import 'package:matrix_gesture_detector/matrix_gesture_detector.dart';
// import 'package:flutter/animation.dart';
import 'package:animations/animations.dart';

class imgDownload extends StatefulWidget {
  final String url;
  imgDownload(this.url);
  @override
  _imgDownloadState createState() => _imgDownloadState();
}
class _imgDownloadState extends State<imgDownload> with TickerProviderStateMixin {
  double _progress = 0;
  bool _poped = false;
  double _opacity = 1.0;
  var _imageFile;
  String _url;
  bool _scale = false;
  bool iconDisabled = false;
  // creating initState()
  @override
  void initState() {
    _url = widget.url;
    super.initState();
  }
  @override
  void setState(fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void dispose() {
    super.dispose();
  }
  void startTimer() {
    _progress = 0;
    _poped = false;
    Timer.periodic(
      Duration(milliseconds: 150),
          (Timer timer) =>
          setState(
                () {
              if (_progress >= 1) {
                timer.cancel();
                // _visible = false;
                _opacity = 1.0;
                _scale = false;
                _poped = true;


              } else if (_progress < 1) {
                _progress += 0.02;
                _scale = !_scale;

              };
              if(_poped) {
                showModalBottomSheet<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        height: 100,
                        color: Colors.blue,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment
                              .spaceEvenly,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment
                                  .spaceBetween,
                              children: [
                                Text(
                                  '   Successfully Downloaded!!!!!!',
                                  style: TextStyle(
                                    // fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),
                                IconButton(
                                  icon: const Icon(
                                      Icons.close,
                                      color: Colors.white
                                  ),
                                  onPressed: () =>
                                      Navigator.pop(context),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    }
                );
              }
            },
          ),
    );
  }

  void _onImageSaveButtonPressed(String url) async {

  }
  // end borrowed code

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(15.0),
            width: 200.0,
            height: 150.0,
            child: Stack( // assets/images/gf.png  'http://www.pixelstalk.net/wp-content/uploads/2016/10/Cool-hd-wallpapers-1080p.jpg'
                children: [
                  Image.network(_url,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Transform.scale(
                        scale: _scale ? 1.2 : 1.0,
                        child: IconButton(
                            icon: Icon(
                              Icons.file_download,
                              color: Colors.deepPurple[900],
                              size: 30,
                            ),
                            onPressed: () {
                              if (!iconDisabled) {
                                _onImageSaveButtonPressed(_url);
                                startTimer();
                                setState(() {
                                  _scale = true;
                                  iconDisabled = true;
                                }
                                );
                              };
                            }
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      LinearProgressIndicator(
                        minHeight: 6.0,
                        backgroundColor: Colors.cyanAccent,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Colors.purple),
                        value: _progress, // determines length of the progress indicator color, so varies
                      ),
                    ],
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}
