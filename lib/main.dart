import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'img_download.dart';
void main() {
  runApp(
    MaterialApp(
      home: TestingFadeThrough(),
    ),
  );
}
class TestingFadeThrough extends StatefulWidget {
  @override
  _TestingFadeThroughState createState() => _TestingFadeThroughState();
}
class _TestingFadeThroughState extends State<TestingFadeThrough> {
  int pageIndex = 0;
  String url;
  static Widget getImageWidgets(String aUrl)
  {
    return imgDownload(aUrl);
  }
  static List<String> _imagesA = [
    'https://tse4.mm.bing.net/th?id=OIP.lZj_WgEZ9lRPaNm2knlOdwHaE9&pid=Api&P=0&w=274&h=184',
    'https://tse2.mm.bing.net/th?id=OIP.XfcBiQpKFAtsaMvxAR-tNgHaEj&pid=Api&P=0&w=278&h=172',
    'https://tse3.mm.bing.net/th?id=OIP.Kwwl000gCZA1t-87vAfzLwHaE7&pid=Api&P=0&w=254&h=170',
    'https://tse4.mm.bing.net/th?id=OIP.Kln11dMLwVlFmCHWrUFaRgHaE8&pid=Api&P=0&w=254&h=170',
    'https://tse3.mm.bing.net/th?id=OIP.ZXFH66LHeqeMyrtmopwlkQEsC7&pid=Api&P=0&w=264&h=166',
    'https://tse3.mm.bing.net/th?id=OIP.weItL-aOPfvZzU-1N7r3aQHaEK&pid=Api&P=0&w=308&h=174',
    'https://tse1.mm.bing.net/th?id=OIP.P0b9D6NaRroGKQZ8mGlZ0AHaEo&pid=Api&P=0&w=259&h=162',
    'https://tse1.mm.bing.net/th?id=OIP.RM-EX8oKtP06liIcfRn0dwHaDt&pid=Api&P=0&w=350&h=176',
    'https://tse4.mm.bing.net/th?id=OIP.c-t7LYbKTe5F8BDSBznwhQHaFj&pid=Api&P=0&w=241&h=181',
  ];
  static List<String> _imagesB = [
    'https://tse2.mm.bing.net/th?id=OIP.ZPa_HTYYX8K1PMlEUSW0VAHaEo&pid=Api&P=0&w=247&h=155',
    'https://tse1.mm.bing.net/th?id=OIP.r29FRe9Vnus_Avye9-H9xwHaEK&pid=Api&P=0&w=289&h=163',
    'https://tse1.mm.bing.net/th?id=OIP.S6w63hMiLFDqP44Nw3uqrQHaEK&pid=Api&P=0&w=339&h=191',
    'https://tse1.mm.bing.net/th?id=OIP.pzs3-MkQuqrWepRCR-z_egHaDt&pid=Api&P=0&w=306&h=153',
    'https://tse4.mm.bing.net/th?id=OIP.nSuJ6lmwC3b5jxJyjItegAHaE7&pid=Api&P=0&w=230&h=154',
    'https://tse3.mm.bing.net/th?id=OIP.f7vDy-s_9HDKUyCdnOjc4gHaFj&pid=Api&P=0&w=267&h=201',
    'https://tse2.mm.bing.net/th?id=OIP.ejFpH2c5aV4K1JhFi9hAfQHaDN&pid=Api&P=0&w=370&h=161',
    'https://tse3.mm.bing.net/th?id=OIP.zT-Kg2rwl6brDXs0Dgvp6wHaDt&pid=Api&P=0&w=321&h=161',
    'https://tse3.mm.bing.net/th?id=OIP.DIN4lkNZF9m-5mrUPEORjgHaFf&pid=Api&P=0&w=211&h=157',
  ];

  List<Widget> pageList = <Widget>[
    Container(key: UniqueKey(),child:GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 0.0,
      mainAxisSpacing: 2.0,
      children: [
        for(var aUrl in _imagesA)getImageWidgets(aUrl),
      ],
    )),
    Container(key: UniqueKey(),child:GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 0.0,
      mainAxisSpacing: 2.0,
      children: [
        for(var aUrl in _imagesB)getImageWidgets(aUrl),
      ],
    ))
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Image Downloader')),
      body: PageTransitionSwitcher(
        transitionBuilder: (
            Widget child,
            Animation<double> animation,
            Animation<double> secondaryAnimation
            ){
          return FadeThroughTransition(
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            child: child,
          );
        },
        child: pageList[pageIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: pageIndex,
        onTap: (int newValue) {
          setState(() {
            pageIndex = newValue;
          });
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.looks_one),
            title: Text('First Page'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.looks_two),
            title: Text('Second Page'),
          )
        ],
      ),
    );
  }
}